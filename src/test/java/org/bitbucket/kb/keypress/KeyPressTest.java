package org.bitbucket.kb.keypress;

import static org.junit.Assert.*;

import org.bitbucket.kb.keypress.KeyModifier;
import org.bitbucket.kb.keypress.KeyPress;
import org.junit.Test;


public class KeyPressTest {

	@Test
	public void testBuild() throws Exception
	{
		KeyPress build = new KeyPress.Builder()
			.character('a')
			.modifier(KeyModifier.ALT)
			.modifier(KeyModifier.CTRL)
			.build();
		assertEquals("<C-A-a>", build.toVimString());
	}

	@Test
	public void testFromVimString() throws Exception
	{
		assertEquals("<C-A-a>", KeyPress.fromVimString("<C-a-a>").toVimString());
		assertEquals("A", KeyPress.fromVimString("A").toVimString());
		assertTrue(KeyPress.fromVimString("<S-a>").equalsVimString("A"));
		assertTrue(KeyPress.fromVimString("<S-a>").equalsVimString("<S-a>"));
		assertTrue(KeyPress.fromVimString("<S-a>").equalsVimString("<S-A>"));
		assertTrue(KeyPress.fromVimString("<S-A>").equalsVimString("A"));
		assertTrue(KeyPress.fromVimString("<S-a>").equalsVimString("<S-a>"));
		assertTrue(KeyPress.fromVimString("A").equalsVimString("<S-A>"));
	}

}
