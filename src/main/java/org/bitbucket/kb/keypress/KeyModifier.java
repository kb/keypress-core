package org.bitbucket.kb.keypress;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public enum KeyModifier implements Comparable<KeyModifier> {
	CTRL("C"),
	SHIFT("S"),
	META("M"),
	ALT("A"),
	MAC_CMD("D"), ;
	public final String vim;

	private static final Map<String, KeyModifier> fromVim;
	static
	{
		Builder<String, KeyModifier> map = ImmutableMap.builder();
		for (KeyModifier mod : values())
		{
			map.put(mod.vim.toLowerCase(), mod);
		}
		fromVim = map.build();
	}

	public static KeyModifier fromVim(String k)
	{
		return fromVim.get(k.toLowerCase());
	}

	private KeyModifier(String vim)
	{
		this.vim = vim;
	}
}